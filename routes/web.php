<?php

use App\Http\Controllers\RegisterWizardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('register-wizards.step-one');
});

Route::get('step-one', [RegisterWizardController::class, 'step_one'])->name('register.step-one');
Route::post('step-one-process', [RegisterWizardController::class, 'step_one_process'])->name('register.step-one-process');

Route::get('step-two', [RegisterWizardController::class, 'step_two'])->name('register.step-two');
Route::post('step-two-process', [RegisterWizardController::class, 'step_two_process'])->name('register.step-two-process');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
