@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Register</div>
        <div class="card-body">
            <form action="{{ route('register.step-two-process') }}" 
                method="POST">
                @csrf
                <div class="form-group">
                    <label for="first_name">Address</label>
                    <textarea name="address" id="address" class="form-control"></textarea>
                
                    @error('first_name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="birth_date">Birth Date</label>
                    <input id="birth_date" class="form-control @error('birth_date') is-invalid @enderror" 
                        type="date" name="birth_date" value="{{ old('birth_date') }}">
                
                    @error('birth_date')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="membership">Membership</label>
                    <select name="membership" id="membership" class="form-control membership @error('membership') is-invalid @enderror">
                        <option value="">-- Pilih Membership --</option>
                        <option value="silver" {{ old('membership') == 'silver' ? 'selected' : '' }}>Silver</option>
                        <option value="gold" {{ old('membership') == 'gold' ? 'selected' : '' }}>Gold</option>
                        <option value="platinum" {{ old('membership') == 'platinum' ? 'selected' : '' }}>Platinum</option>
                        <option value="black" {{ old('membership') == 'black' ? 'selected' : '' }}>Black</option>
                        <option value="vip" {{ old('membership') == 'vip' ? 'selected' : '' }}>VIP</option>
                        <option value="vvip" {{ old('membership') == 'vvip' ? 'selected' : '' }}>VVIP</option>
                    </select>
                
                    @error('membership')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="credit_card">Credit Card</label>
                    <input id="credit_card" class="form-control @error('credit_card') is-invalid @enderror" 
                        type="text" name="credit_card" value="{{ old('credit_card') }}">
                
                    @error('credit_card')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('register.step-one') }}" class="btn btn-primary">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
