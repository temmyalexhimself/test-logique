@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.message')
    <div class="card">
        <div class="card-header">Register</div>
        <div class="card-body">
            <form action="{{ route('register.step-one-process') }}" 
                method="POST">
                @csrf
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input id="first_name" class="form-control @error('first_name') is-invalid @enderror" 
                        type="text" name="first_name" value="{{ $user->first_name ?? '' }}">
                
                    @error('first_name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input id="last_name" class="form-control @error('last_name') is-invalid @enderror" 
                        type="text" name="last_name" value="{{ $user->last_name ?? '' }}">
                
                    @error('last_name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" class="form-control @error('email') is-invalid @enderror" 
                        type="text" name="email" value="{{ $user->email ?? '' }}">
                
                    @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" class="form-control @error('password') is-invalid @enderror" 
                        type="password" name="password" value="{{ old('password') }}">
                
                    @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Password Confirmation</label>
                    <input id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" 
                        type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
                
                    @error('password_confirmation')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary">Next</button>
            </form>
        </div>
    </div>
</div>
@endsection


