<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterWizardController extends Controller
{
    public function step_one(Request $request)
    {
        $user = $request->session()->get('users');
        return view('register-wizards.step-one', ['user' => $user]);
    }

    public function step_one_process(Request $request)
    {
        $request->validate([
            'last_name' => 'required',
            'password' => 'required|confirmed',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
        ]);

        if (empty($request->session()->get('users'))) {
            $user = new User();
            $request->session()->put('users', $user);
        } else {
            $user = $request->session()->get('users');
            $request->session()->put('users', $user);
        }

        return redirect()->route('register.step-two');
    }

    public function step_two(Request $request)
    {
        $user = $request->session()->get('users');
        return view('register-wizards.step-two', ['user' => $user]);
    }

    public function step_two_process(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'birth_date' => 'required',
            'membership' => 'required',
            // 'credit_card' => 'required|regex:(5[1-5]\d{14})'
            'credit_card' => 'required'
        ]);

        $user = $request->session()->get('users');
        User::create([
            'first_name' => $user->nama_depan,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'password' => bcrypt($user->password),
            'birth_date' => $request->birth_date,
            'membership' => $request->membership,
            'credit_card' => $request->credit_card,
            'address' => $request->address,
        ]);

        $request->session()->forget('users');

        return redirect()->route('register.step-one');
    }
}
